import React, { useState, useEffect } from 'react';
import { useQuery } from '@tanstack/react-query';
import axios from 'axios';
import Coin from './Coin';

const URL = 'https://mud-time-glazer.glitch.me';

const Home = () => {
    const [coins, setCoins] = useState([]);
    const [search, setSearch] = useState('usdt');

    const { status, data, error, isFetching } = useQuery(["coins"], async () => {
        const { data } = await axios.get(URL + '/list')
            .then(res => {
                setCoins(res.data.data.filter(coin =>
                    coin.symbol.toLowerCase().includes(search.toLowerCase())
                ));
            })
            .catch(error => console.log(error));
        return data
    })
    // info-spot/${name}/30m
    const handleRouteName = (name) => {
        return axios.get(URL + `info-spot/${name}/30m`)
    }

    const handleChange = e => {
        setSearch(e.target.value);
    };

    return (
        <div className='coin-app'>
            <div className='coin-search'>
                <h1 className='coin-text'>Search a currency</h1>
                <form>
                    <input
                        className='coin-input'
                        type='text'
                        onChange={handleChange}
                        placeholder='Search'
                    />
                </form>
            </div>
            {coins.map(coin => {
                return (
                    <a href='#' onClick={handleRouteName(coin.symbol)}>
                        <Coin
                            symbol={coin.symbol}
                        />
                    </a>
                );
            })}
        </div>
    )
}
export default Home