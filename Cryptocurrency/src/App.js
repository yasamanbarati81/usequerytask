import React, { Fragment } from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import './App.css';
import Home from './component/Home';

const queryClient = new QueryClient();

function App() {

  return (
    <Fragment>
      <QueryClientProvider client={queryClient}>
        <Home />
      </QueryClientProvider>
    </Fragment>
  );
}

export default App;
