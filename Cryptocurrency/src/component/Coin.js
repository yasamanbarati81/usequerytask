import React from 'react';
import './Coin.css';
// "symbol": "APEBUSD",
// "priceChange": "-0.5380000",
// "priceChangePercent": "-7.388",
// "weightedAvgPrice": "6.9331175",
// "lastPrice": "6.7440000",
// "lastQty": "49.6",
// "openPrice": "7.2820000",
// "highPrice": "7.2900000",
// "lowPrice": "6.6520000",
// "volume": "3553473.6",
// "quoteVolume": "24636650.1800000",
// "openTime": 1660033740000,
// "closeTime": 1660120173312,
// "firstId": 7946039,
// "lastId": 8014584,
// "count": 68546
const Coin = ({
  symbol
}) => {
  return (
    <div className='coin-container'>
      <div className='coin-row'>
        <div className='coin'>
          {/* <img src={image} alt='crypto' /> */}
          <p className='coin-symbol'>{symbol}</p>
        </div>
        {/* <div className='coin-data'>
          <p className='coin-price'>${price}</p>
          <p className='coin-volume'>${volume.toLocaleString()}</p>

          {priceChange < 0 ? (
            <p className='coin-percent red'>{priceChange.toFixed(2)}%</p>
          ) : (
            <p className='coin-percent green'>{priceChange.toFixed(2)}%</p>
          )}

          <p className='coin-marketcap'>
            Mkt Cap: ${marketcap.toLocaleString()}
          </p>
        </div> */}
      </div>
    </div>
  );
};

export default Coin;
