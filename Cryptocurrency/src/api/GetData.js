import { useQuery } from '@tanstack/react-query';
import axios from 'axios';

export function GetData({ endpoint }){
    return useQuery(["coins"], () => {
        return axios.get(endpoint).then(res => res.data)
    })
}
